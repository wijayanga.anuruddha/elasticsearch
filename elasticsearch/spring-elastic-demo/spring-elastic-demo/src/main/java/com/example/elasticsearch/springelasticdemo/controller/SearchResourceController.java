package com.example.elasticsearch.springelasticdemo.controller;

import com.example.elasticsearch.springelasticdemo.model.Users;
import com.example.elasticsearch.springelasticdemo.service.UserService;
import com.example.elasticsearch.springelasticdemo.util.CustomResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchResourceController
{

	@PostMapping("/users")
	public CustomResponse saveUser( @RequestBody Users users )
	{
		CustomResponse customResponse = null;

		try
		{
			UserService.saveUserToES( users );
			customResponse = new CustomResponse( 200, "success", null );
		}
		catch ( Exception e )
		{
			customResponse = new CustomResponse( 200, e.getMessage(), null );
		}
		return customResponse;
	}

	@GetMapping("/users")
	public CustomResponse retrieveUsers( @RequestParam("id") long id )
	{
		CustomResponse customResponse = null;

		try
		{
			Object[] object = UserService.getUserFromES( id );
			customResponse = new CustomResponse( 200, "success", object[1] );
		}
		catch ( Exception e )
		{
			customResponse = new CustomResponse( 200, e.getMessage(), null );
		}
		return customResponse;
	}

	@DeleteMapping("/users")
	public CustomResponse deleteUser( @RequestParam("id") String id )
	{
		CustomResponse customResponse = null;

		try
		{
			UserService.deleteUsersInES( id );
			customResponse = new CustomResponse( 200, "success", null );
		}
		catch ( Exception e )
		{
			customResponse = new CustomResponse( 200, e.getMessage(), null );
		}
		return customResponse;
	}

}
