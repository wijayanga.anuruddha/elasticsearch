package com.example.elasticsearch.springelasticdemo.service;

import com.example.elasticsearch.springelasticdemo.ElasticClient;
import com.example.elasticsearch.springelasticdemo.model.Users;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;

@Service
public class UserService
{
	final static String INDEX = "es_user";
	final static String USERS = "users";

	public static void saveUserToES( Users users )
	{
		try
		{
			Client elasticSearchClient = ElasticClient.getElasticsearchClient();
			IndexRequest indexRequest = new IndexRequest( INDEX, USERS );
			indexRequest.id( String.valueOf( users.getId() ) );
			indexRequest.source( new Gson().toJson( users ) );

			IndexResponse response1 = elasticSearchClient.index( indexRequest ).actionGet();
			System.out.println( response1 );
		}
		catch ( Exception ex )
		{
			ex.printStackTrace();
		}
	}

	public static Object[] getUserFromES( long userId )
	{
		Object[] userIdAndUser = new Object[2];

		try
		{
			String id = null;
			Users users = null;

			Client elasticSearchClient = ElasticClient.getElasticsearchClient();

			BoolQueryBuilder query = QueryBuilders.boolQuery();
			query.must( QueryBuilders.matchQuery( "id", userId ) );

			SearchResponse getResponse = elasticSearchClient.prepareSearch( INDEX ).setTypes( USERS )
					.setQuery( query )
					.get();

			for ( SearchHit searchHitFields : getResponse.getHits() )
			{
				id = searchHitFields.getId();
				users = new Gson().fromJson( new GsonBuilder().setPrettyPrinting().create().toJson( searchHitFields.getSource() ), Users.class );
			}
			userIdAndUser[0] = id;
			userIdAndUser[1] = users;
		}
		catch ( NoNodeAvailableException ex )
		{
			ex.printStackTrace();
		}
		catch ( UnknownHostException ex )
		{
			ex.printStackTrace();
		}

		return userIdAndUser;
	}

	public static void deleteUsersInES( String id )
	{
		try
		{
			Client client = ElasticClient.getElasticsearchClient();

			DeleteResponse response = client.prepareDelete( INDEX,
					USERS
					, id )
					.get();

			System.out.println( response.status() );
		}
		catch ( NoNodeAvailableException ex )
		{
			ex.printStackTrace();
		}
		catch ( UnknownHostException ex )
		{
			ex.printStackTrace();
		}
	}

	public static void updateUser( Users users )
	{
		try
		{
			//			deleteUsersInES( users );

		}
		catch ( Exception ex )
		{
			ex.printStackTrace();
		}
	}
}
