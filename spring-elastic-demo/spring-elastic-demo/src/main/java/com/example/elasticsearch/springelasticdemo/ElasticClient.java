package com.example.elasticsearch.springelasticdemo;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ElasticClient
{
	private static Client client = null;

	final static public String ELASTIC_HOST = "127.0.0.1";
	final static public long ELASTIC_PORT = 9300;
	final static public String CLUSTER_NAME = "elasticsearch";

	private ElasticClient()
	{
	}

	public static Client getElasticsearchClient() throws UnknownHostException
	{

		if ( client == null )
		{
			client = new PreBuiltTransportClient(
					Settings.builder().put( "client.transport.sniff", true )
							.put( "cluster.name", CLUSTER_NAME ).build() )
					.addTransportAddress( new InetSocketTransportAddress( InetAddress.getByName( ELASTIC_HOST ), ( int ) ELASTIC_PORT ) );
		}
		return client;
	}
}
