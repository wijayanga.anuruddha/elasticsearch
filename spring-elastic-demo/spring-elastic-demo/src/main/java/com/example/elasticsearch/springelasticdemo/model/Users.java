package com.example.elasticsearch.springelasticdemo.model;


public class Users
{
	private long id ;
	private String name;
	private String teamName;
	private long salary;

	public Users()
	{
	}

	public Users( long id, String name, String teamName, long salary )
	{
		this.id = id;
		this.name = name;
		this.teamName = teamName;
		this.salary = salary;
	}

	public long getId()
	{
		return id;
	}

	public void setId( long id )
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName( String name )
	{
		this.name = name;
	}

	public String getTeamName()
	{
		return teamName;
	}

	public void setTeamName( String teamName )
	{
		this.teamName = teamName;
	}

	public long getSalary()
	{
		return salary;
	}

	public void setSalary( long salary )
	{
		this.salary = salary;
	}
}
