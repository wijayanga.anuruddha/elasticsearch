package com.example.elasticsearch.springelasticdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringElasticDemoApplication
{

	public static void main( String[] args )
	{
		SpringApplication.run( SpringElasticDemoApplication.class, args );
	}

}
